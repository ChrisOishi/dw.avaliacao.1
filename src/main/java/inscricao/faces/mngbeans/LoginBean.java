/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import java.util.ArrayList;
import java.util.Date;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;
import utfpr.faces.support.PageBean;

/**
 *
 * @author chris
 */
@Named
@RequestScoped
public class LoginBean extends PageBean {
    private String usuario;
    private String senha;
    private boolean opadmin;
    private ArrayList<logins> logins = new ArrayList<logins>();
    
    public String getUsuario(){
        return usuario;
    }
    public void setUsuario(String usuario){
        this.usuario = usuario;
    }
    public String getSenha(){
        return senha;
    }
    public void setSenha(String senha){
        this.senha = senha;
    }
    public boolean getOpadmin(){
        return opadmin;
    }
    public void setOpadmin(boolean opadmin){
        this.opadmin = opadmin;
    }
    public String confirmaAction() {
        if(usuario == senha){
            logins.add(new logins(usuario,new Date()));
            if(opadmin){
                return "admin";
            }
            else return "cadastro";
        }
        else{
            FacesMessage msg = new FacesMessage("Acesso negado"); 
            throw new ValidatorException(msg);
        }
    }
    
}
